===============
READ ME - tests
===============

This directory, ``tests/``, is for automated tests.

Every file in this directory must be a valid *pytest* test file.
