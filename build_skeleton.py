"""Build a skeleton for an new Python package.

.. moduleauthor:: Michael Rippstein <info@anatas.ch>

:copyright: Copyright 2024 by Michael Rippstein
:license: GNU General Public License v3 or later (GPLv3+), see LICENSE for details.
"""

from __future__ import annotations

import argparse
import tomllib
from collections.abc import Sequence
from dataclasses import dataclass
from enum import Enum
from pathlib import Path, PurePosixPath
from pprint import pprint

from jinja2 import Environment, FileSystemLoader

TEMPLATE_BASE = Path('template')
SOURCE_DIRECTORY = 'src'
DOCUMENTATION_DIRECTORY = 'docs'
TESTS_DIRECTORY = 'tests'
VSCODE_DIRECTORY = '_vscode'
ROOT_DIRECTORY = '.'


class LicenseType(Enum):
    AGPL3 = (
        'AGPLv3',
        'License :: OSI Approved :: GNU Affero General Public License v3 (AGPLv3)',
        'GNU Affero General Public License v3 (AGPLv3)',
    )
    AGPL3P = (
        'AGPLv3+',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'GNU Affero General Public License v3 or later (AGPLv3+)',
    )
    GPL2 = (
        'GPLv2',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'GNU General Public License v2 (GPLv2)',
    )
    GPL3 = (
        'GPLv3',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'GNU General Public License v3 (GPLv3)',
    )
    GPL3P = (
        'GPLv3+',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'GNU General Public License v3 or later (GPLv3+)',
    )

    def __init__(self, short: str, long: str, classifier: str) -> None:
        self.short = short
        self.long = long
        self.classifier = classifier

    @classmethod
    def from_string(cls, value: str) -> LicenseType:
        match value.upper():
            case 'AGPLV3' | 'AGPL3':
                return cls.AGPL3
            case 'AGPLV3+' | 'AGPL3+' | 'AGPLV3P' | 'AGPL3P':
                return cls.AGPL3P
            case 'GPLV2' | 'GPL2':
                return cls.GPL2
            case 'GPLV3' | 'GPL3':
                return cls.GPL3
            case 'GPLV3+' | 'GPL3+' | 'GPLV3P' | 'GPL3P':
                return cls.GPL3P
            case _:
                raise ValueError(f'Do not know hot to convert {value}.')


@dataclass
class Author:
    name: str
    email: str


@dataclass
class Url:
    title: str
    url: str


@dataclass
class Project:
    name: str
    description: str
    year: str
    urls: Sequence[Url]
    classifiers: Sequence[str]


@dataclass
class License:
    type: LicenseType
    short: str
    long: str
    classifier: str


def output_src_directory(
    output_base_directory: Path, project: Project, author: Author, license_: License
) -> None:
    input_dir = TEMPLATE_BASE.resolve() / SOURCE_DIRECTORY
    print(input_dir)
    output_dir = output_base_directory / SOURCE_DIRECTORY
    input_files = list(input_dir.glob('**/*.py_t'))
    print(input_files)
    env = Environment(loader=FileSystemLoader(input_dir))
    for in_file in input_files:
        print(in_file)
        directory = output_dir / in_file.relative_to(input_dir).parent
        directory.mkdir(parents=True, exist_ok=True)
        template = env.get_template(str(PurePosixPath(in_file.relative_to(input_dir))))
        print(template.render(project=project, author=author, license=license_))
    pass


def output_doc_directory(output_directory: Path) -> None:
    pass


def output_test_directory(output_directory: Path) -> None:
    pass


def output_root_directory(output_directory: Path) -> None:
    pass


def output_vscode_directory(output_directory: Path) -> None:
    pass


def main() -> None:
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        '-p', '--project', help='Project, package name. Must be a valid python package name.'
    )
    argument_parser.add_argument('-y', '--year', help='The year information for the copyright texts.')
    argument_parser.add_argument(
        '-l',
        '--license',
        help='Chose the license for the project.',
        choices=['AGPLv3', 'AGPLv3P', 'GPLv2', 'GLPv3', 'GPLv3P'],
    )
    argument_parser.add_argument('-a', '--author', help='Author of the package.')
    argument_parser.add_argument('-e', '--email', help='Email address of the author.')
    argument_parser.add_argument(
        '-c',
        '--config-file',
        help='The config file. The config file values will over written by the command line arguments.',
    )
    argument_parser.add_argument('-o', '--output', help='The base directory for the new package.')
    arguments = argument_parser.parse_args()
    # validate the input
    if arguments.config_file is not None:
        with open(arguments.config_file, 'rb') as f:
            config_dict = tomllib.load(f)
    else:
        config_dict = {'author': {}, 'project': {}, 'license': {}}
    # author
    if arguments.author is not None:
        author_name = arguments.author
    else:
        author_name = config_dict['author'].get('name', 'No Author')
    if arguments.email is not None:
        author_email = arguments.email
    else:
        author_email = config_dict['author'].get('email', 'No E-Mail')

    author = Author(
        name=author_name,
        email=author_email,
    )
    # license
    try:
        license_type = LicenseType.from_string(arguments.license if arguments.license is not None else '')
    except ValueError:
        license_type = None
    if license_type is None:
        short = config_dict['license'].get('short', 'other license')
        long = config_dict['license'].get('long', 'other license')
        classifier = config_dict['license'].get('classifier', 'other license')
    else:
        short = license_type.short
        long = license_type.long
        classifier = license_type.classifier
    license_info = License(
        type=license_type,
        short=short,
        long=long,
        classifier=classifier,
    )
    # project

    base_directory = Path(arguments.output) if arguments.output is not None else Path.cwd()
    pprint(arguments)
    pprint(author)

    output_src_directory(base_directory, author=author, license_=license_info)


if __name__ == '__main__':
    main()
